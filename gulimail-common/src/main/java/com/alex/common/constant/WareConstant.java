package com.alex.common.constant;

/**
 * @author AlexDing
 * @create 2021-11-03 上午 1:23
 */
public class WareConstant {

    /**
     * 采购单枚举
     */
    public enum PurchaseStatusEnum{
        CREATE(0,"新建"),
        ASSIGNED(1,"已分配"),
        RECEIVE(2,"已领取"),
        FINISH(3,"已完成"),
        HAS_ERROR(4,"有异常");

        private Integer code;
        private String message;

        PurchaseStatusEnum(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }

    /**
     * 采购需求枚举
     */
    public enum PurchaseDetailStatusEnum{
        CREATE(0,"新建"),
        ASSIGNED(1,"已分配"),
        BUYING(2,"正在采购"),
        COMPLETE(3,"已完成"),
        HAS_ERROR(4,"采购失败");

        private Integer code;
        private String message;

        PurchaseDetailStatusEnum(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }
}
