package com.alex.common.to;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author AlexDing
 * @create 2021-11-02 上午 1:07
 */
@Data
public class SkuReductionTo {

    private Long skuId;
    /**
     * 满减信息
     */
    private Integer fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    /**
     * 会员价格
     */
    private List<MemberPrice> memberPrice;
}
