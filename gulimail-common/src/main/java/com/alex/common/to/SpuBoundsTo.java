package com.alex.common.to;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author AlexDing
 * @create 2021-11-02 上午 0:41
 */
@Data
public class SpuBoundsTo implements Serializable {
    private Long spuId;
    private BigDecimal growBounds;
    private BigDecimal buyBounds;
}
