package com.alex.gulimail.order.dao;

import com.alex.gulimail.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author AlexDing
 * @email alexding@gmail.com
 * @date 2021-10-23 13:13:59
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
