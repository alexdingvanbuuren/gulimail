package com.alex.gulimail.coupon.dao;

import com.alex.gulimail.coupon.entity.SeckillSkuNoticeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀商品通知订阅
 * 
 * @author AlexDing
 * @email alexding@gmail.com
 * @date 2021-10-23 13:20:59
 */
@Mapper
public interface SeckillSkuNoticeDao extends BaseMapper<SeckillSkuNoticeEntity> {
	
}
