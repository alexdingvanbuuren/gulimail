package com.alex.gulimail.coupon.service.impl;

import com.alex.common.to.MemberPrice;
import com.alex.common.to.SkuReductionTo;
import com.alex.gulimail.coupon.entity.MemberPriceEntity;
import com.alex.gulimail.coupon.entity.SkuLadderEntity;
import com.alex.gulimail.coupon.service.MemberPriceService;
import com.alex.gulimail.coupon.service.SkuLadderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.alex.common.utils.PageUtils;
import com.alex.common.utils.Query;

import com.alex.gulimail.coupon.dao.SkuFullReductionDao;
import com.alex.gulimail.coupon.entity.SkuFullReductionEntity;
import com.alex.gulimail.coupon.service.SkuFullReductionService;
import org.springframework.transaction.annotation.Transactional;


@Service("skuFullReductionService")
public class SkuFullReductionServiceImpl extends ServiceImpl<SkuFullReductionDao, SkuFullReductionEntity> implements SkuFullReductionService {

    @Autowired
    private SkuLadderService skuLadderService;

    @Autowired
    private MemberPriceService memberPriceService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuFullReductionEntity> page = this.page(
                new Query<SkuFullReductionEntity>().getPage(params),
                new QueryWrapper<SkuFullReductionEntity>()
        );

        return new PageUtils(page);
    }


    @Transactional
    @Override
    public void saveReduction(SkuReductionTo skuReductionTo) {
        //保存优惠、打折、会员价信息
        //操作的库：gulimail_sms，sms_sku_ladder：打折表
        SkuLadderEntity ladderEntity = new SkuLadderEntity();
        ladderEntity.setFullCount(skuReductionTo.getFullCount());
        ladderEntity.setDiscount(skuReductionTo.getDiscount());
        ladderEntity.setAddOther(skuReductionTo.getCountStatus());
        ladderEntity.setSkuId(skuReductionTo.getSkuId());
        if(skuReductionTo.getFullCount() > 0){
            skuLadderService.save(ladderEntity);
        }

        //操作的库：gulimail_sms，sms_sku_full_reduction：满减表
        SkuFullReductionEntity reductionEntity = new SkuFullReductionEntity();
        reductionEntity.setSkuId(skuReductionTo.getSkuId());
        reductionEntity.setFullPrice(skuReductionTo.getFullPrice());
        reductionEntity.setReducePrice(skuReductionTo.getReducePrice());
        reductionEntity.setAddOther(skuReductionTo.getPriceStatus());
        if(skuReductionTo.getFullPrice().compareTo(BigDecimal.ZERO) == 1){
            this.save(reductionEntity);
        }

        //操作的库：gulimail_sms，sms_member_price：会员价格表
        List<MemberPrice> memberPriceList = skuReductionTo.getMemberPrice();
        if(memberPriceList != null && memberPriceList.size() > 0){
            List<MemberPriceEntity> memberPriceEntities = memberPriceList.stream().map(item -> {
                MemberPriceEntity priceEntity = new MemberPriceEntity();
                priceEntity.setSkuId(skuReductionTo.getSkuId());
                priceEntity.setMemberLevelId(item.getId());
                priceEntity.setMemberLevelName(item.getName());
                priceEntity.setMemberPrice(item.getPrice());
                priceEntity.setAddOther(1);
                return priceEntity;
            }).filter(item -> {
                return item.getMemberPrice().compareTo(BigDecimal.ZERO) == 1;
            }).collect(Collectors.toList());
            memberPriceService.saveBatch(memberPriceEntities);
        }
    }

}