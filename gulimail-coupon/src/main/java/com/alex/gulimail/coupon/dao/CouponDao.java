package com.alex.gulimail.coupon.dao;

import com.alex.gulimail.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author AlexDing
 * @email alexding@gmail.com
 * @date 2021-10-23 13:20:59
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
