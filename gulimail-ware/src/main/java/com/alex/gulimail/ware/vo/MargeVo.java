package com.alex.gulimail.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author AlexDing
 * @create 2021-11-03 上午 1:13
 */
@Data
public class MargeVo {
    /**
     * 整单id
     */
    private Long purchaseId;
    /**
     * 合并项集合
     */
    private List<Long> items;
}
