package com.alex.gulimail.ware.dao;

import com.alex.gulimail.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author AlexDing
 * @email alexding@gmail.com
 * @date 2021-10-23 13:26:54
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
