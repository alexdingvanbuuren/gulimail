package com.alex.gulimail.ware.vo;

import lombok.Data;

/**
 * @author AlexDing
 * @create 2021-11-03 下午 22:58
 */
@Data
public class PurchaseItemDoneVo {
    /**
     * 采购项id
     */
    private Long itemId;
    /**
     * 采购项状态
     */
    private Integer status;
    /**
     * 采购结果
     */
    private String reason;
}
