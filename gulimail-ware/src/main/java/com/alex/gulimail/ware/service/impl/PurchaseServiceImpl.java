package com.alex.gulimail.ware.service.impl;

import com.alex.common.constant.WareConstant;
import com.alex.gulimail.ware.entity.PurchaseDetailEntity;
import com.alex.gulimail.ware.service.PurchaseDetailService;
import com.alex.gulimail.ware.service.WareSkuService;
import com.alex.gulimail.ware.vo.MargeVo;
import com.alex.gulimail.ware.vo.PurchaseDoneVo;
import com.alex.gulimail.ware.vo.PurchaseItemDoneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.alex.common.utils.PageUtils;
import com.alex.common.utils.Query;

import com.alex.gulimail.ware.dao.PurchaseDao;
import com.alex.gulimail.ware.entity.PurchaseEntity;
import com.alex.gulimail.ware.service.PurchaseService;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;


@Service("purchaseService")
public class PurchaseServiceImpl extends ServiceImpl<PurchaseDao, PurchaseEntity> implements PurchaseService {

    @Autowired
    private PurchaseDetailService purchaseDetailService;

    @Autowired
    private WareSkuService wareSkuService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageUnReceivePurchaseList(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>().in("status",
                        WareConstant.PurchaseStatusEnum.CREATE.getCode(),
                        WareConstant.PurchaseStatusEnum.ASSIGNED.getCode())
        );

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void margePurchase(MargeVo margeVo) {
        Long purchaseId = margeVo.getPurchaseId();
        if(purchaseId == null){
            //1.新建采购单
            PurchaseEntity purchaseEntity = new PurchaseEntity();
            purchaseEntity.setCreateTime(new Date());
            purchaseEntity.setUpdateTime(new Date());
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.CREATE.getCode());
            this.save(purchaseEntity);
            purchaseId = purchaseEntity.getId();
        }

        //合并采购需求
        List<Long> items = margeVo.getItems();
        Long finalPurchaseId = purchaseId;

        List<PurchaseDetailEntity> detailEntityList = items.stream().map(item -> {
            PurchaseDetailEntity detailEntity = purchaseDetailService.getById(item);
            return detailEntity;
        }).filter(entity -> {
            //确认采购单状态是0或1才可以合并
            return entity.getStatus() == WareConstant.PurchaseDetailStatusEnum.CREATE.getCode() ||
                    entity.getStatus() == WareConstant.PurchaseDetailStatusEnum.ASSIGNED.getCode();
        }).map(entity -> {
            PurchaseDetailEntity detailEntity = new PurchaseDetailEntity();
            detailEntity.setId(entity.getId());
            detailEntity.setPurchaseId(finalPurchaseId);
            detailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.ASSIGNED.getCode());
            return detailEntity;
        }).collect(Collectors.toList());

        if(detailEntityList != null && detailEntityList.size() > 0){
            purchaseDetailService.updateBatchById(detailEntityList);
        }

        //更新时间
        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setUpdateTime(new Date());
        purchaseEntity.setId(purchaseId);
        this.updateById(purchaseEntity);
    }

    /**
     * @param purchaseIds 采购单id
     */
    @Override
    public void receivedPurchase(List<Long> purchaseIds) {
        //1.确定当前采购单是新建或已分配状态(后期如果完善采购系统需要确定只能是当前采购人员的采购单)
        List<PurchaseEntity> purchaseEntityList = purchaseIds.stream().map(id -> {
            PurchaseEntity purchaseEntity = this.getById(id);
            return purchaseEntity;
        }).filter(entity -> {
            return entity.getStatus() == WareConstant.PurchaseStatusEnum.CREATE.getCode() ||
                    entity.getStatus() == WareConstant.PurchaseStatusEnum.ASSIGNED.getCode();
        }).map(entity -> {
            //设置采购单状态/更新时间
            entity.setStatus(WareConstant.PurchaseStatusEnum.RECEIVE.getCode());
            entity.setUpdateTime(new Date());
            return entity;
        }).collect(Collectors.toList());

        //2.改变采购单状态
        this.updateBatchById(purchaseEntityList);

        //3.改变采购项的状态
        purchaseEntityList.forEach(purchase -> {
            List<PurchaseDetailEntity> detailEntities = purchaseDetailService.listDetailByPurchaseId(purchase.getId());
            List<PurchaseDetailEntity> collect = detailEntities.stream().map(entity -> {
                PurchaseDetailEntity detailEntity = new PurchaseDetailEntity();
                detailEntity.setId(entity.getId());
                detailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.BUYING.getCode());
                return detailEntity;
            }).collect(Collectors.toList());
            purchaseDetailService.updateBatchById(collect);
        });
    }

    @Transactional
    @Override
    public void donePurchase(PurchaseDoneVo doneVo) {
        //1.改变采购项状态
        Boolean flag = true;
        List<PurchaseItemDoneVo> items = doneVo.getItems();
        List<PurchaseDetailEntity> updates = new ArrayList<>();
        for (PurchaseItemDoneVo item : items) {
            if(item.getStatus() == WareConstant.PurchaseDetailStatusEnum.HAS_ERROR.getCode()){
                //采购项有失败的
                flag = false;
            }else{
                //3.将成功采购进行入库
                PurchaseDetailEntity detailEntity = purchaseDetailService.getById(item.getItemId());
                wareSkuService.addStock(detailEntity.getSkuId(),detailEntity.getSkuNum(),detailEntity.getWareId());
            }
            PurchaseDetailEntity detailEntity = new PurchaseDetailEntity();
            detailEntity.setId(item.getItemId());
            detailEntity.setStatus(item.getStatus());
            detailEntity.setReason(item.getReason());
            updates.add(detailEntity);
        }
        purchaseDetailService.updateBatchById(updates);

        //2.改变采购单状态
        Long purchaseId = doneVo.getId();
        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setId(purchaseId);
        purchaseEntity.setUpdateTime(new Date());
        if(flag){
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.FINISH.getCode());
        }else{
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.HAS_ERROR.getCode());
        }
        this.updateById(purchaseEntity);
    }

}