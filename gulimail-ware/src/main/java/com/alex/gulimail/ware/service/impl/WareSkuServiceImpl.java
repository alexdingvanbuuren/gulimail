package com.alex.gulimail.ware.service.impl;

import com.alex.common.utils.R;
import com.alex.gulimail.ware.feign.ProductServiceFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.alex.common.utils.PageUtils;
import com.alex.common.utils.Query;

import com.alex.gulimail.ware.dao.WareSkuDao;
import com.alex.gulimail.ware.entity.WareSkuEntity;
import com.alex.gulimail.ware.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    @Autowired
    private ProductServiceFeign productServiceFeign;

    /**
     * 检索的条件：
     *  skuId: 1
     *  wareId: 1
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareSkuEntity> queryWrapper = new QueryWrapper<>();

        String skuId = (String) params.get("skuId");
        if(!StringUtils.isEmpty(skuId)){
            queryWrapper.eq("sku_id",skuId);
        }

        String wareId = (String) params.get("wareId");
        if(!StringUtils.isEmpty(wareId)){
            queryWrapper.eq("ware_id",wareId);
        }

        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params), queryWrapper);
        return new PageUtils(page);
    }


    @Override
    public void addStock(Long skuId, Integer skuNum, Long wareId) {
        //判断如果还没有这个库存记录
        List<WareSkuEntity> skuEntities = this.list(new QueryWrapper<WareSkuEntity>()
                .eq("sku_id", skuId).eq("ware_id", wareId));
        if(skuEntities == null || skuEntities.size() == 0){
            //没有库存记录，新增记录
            WareSkuEntity wareSkuEntity = new WareSkuEntity();
            wareSkuEntity.setSkuId(skuId);
            wareSkuEntity.setStock(skuNum);
            wareSkuEntity.setWareId(wareId);
            wareSkuEntity.setStockLocked(0);
            //远程调用product服务查询skuName，如果失败，整个事务无需回滚，有两种做法：
            //第一种做法：只要tryCatch捕获了异常则不会回滚
            //TODO 第二种做法：高级部分解答
            try {
                R info = productServiceFeign.info(skuId);
                if(info.getCode() == 0){
                    Map<String,Object> map = (Map) info.get("skuInfo");
                    String skuNmae = (String) map.get("skuName");
                    wareSkuEntity.setSkuName(skuNmae);
                }
            } catch (Exception exception) {
                //有异常整个事务无需回滚，继续下面操作
            }
            this.baseMapper.insert(wareSkuEntity);
        }else{
            //有库存记录，修改库存数量
            this.baseMapper.addStock(skuId, skuNum, wareId);
        }
    }

}