package com.alex.gulimail.ware.service;

import com.alex.gulimail.ware.vo.MargeVo;
import com.alex.gulimail.ware.vo.PurchaseDoneVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.alex.common.utils.PageUtils;
import com.alex.gulimail.ware.entity.PurchaseEntity;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author AlexDing
 * @email alexding@gmail.com
 * @date 2021-10-23 13:26:54
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnReceivePurchaseList(Map<String, Object> params);

    void margePurchase(MargeVo margeVo);

    void receivedPurchase(List<Long> purchaseIds);

    void donePurchase(PurchaseDoneVo doneVo);
}

