package com.alex.gulimail.ware.feign;

import com.alex.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author AlexDing
 * @create 2021-11-04 上午 0:17
 */
@FeignClient("gulimail-product")
public interface ProductServiceFeign {

    /**
     *      /product/skuinfo/info/{skuId}
     *      /api/product/skuinfo/info/{skuId}
     *
     * 有两种发送请求方式：
     *   1)、让所有请求过网关
     *      1. @FeignClient("gulimail-gateway")：给gulimail-gateway所在的机器发请求
     *      2. 请求路径为：/api/product/skuinfo/info/{skuId}
     *
     *   2)、直接让后台指定服务处理
     *      1. @FeignClient("gulimail-product")：给gulimail-product所在的机器发请求
     *      2. 请求路径为：/product/skuinfo/info/{skuId}
     *
     * @param skuId
     * @return
     */
    @RequestMapping("/product/skuinfo/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId);
}
