package com.alex.gulimail.product;

import com.alex.gulimail.product.entity.BrandEntity;
import com.alex.gulimail.product.service.BrandService;
import com.alex.gulimail.product.service.CategoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
@Slf4j
class GulimailProductApplicationTests {

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Test
    public void testFindPath(){
        Long[] catelogPath = categoryService.findCatelogPath(225L);
        log.info("catelogPath完整路径:{}", Arrays.asList(catelogPath));
    }

    @Test
    public void testSplit(){
        String str = "https://gulimail-alex.oss-cn-guangzhou.aliyuncs.com/2021-11-01/166d9497-b23b-40e5-8823-6c004d1e8787_a83bf5250e14caf2.jpg";
        String substring = str.substring(str.lastIndexOf("/") + 1,str.lastIndexOf("."));
        System.out.println(substring);
    }

    @Test
    void contextLoads() {
//        BrandEntity brandEntity = new BrandEntity();
//        brandEntity.setBrandId(1L);
//        brandEntity.setDescript("华为");

//        brandEntity.setName("华为");
//        brandService.save(brandEntity);
//        System.out.println("保存成功...");

//        brandService.updateById(brandEntity);

        List<BrandEntity> list = brandService.list(
                new QueryWrapper<BrandEntity>().eq("name","华为")
        );
        for (BrandEntity brandEntity : list) {
            System.out.println(brandEntity);
        }
    }

}
