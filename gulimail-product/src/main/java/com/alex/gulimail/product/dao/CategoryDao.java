package com.alex.gulimail.product.dao;

import com.alex.gulimail.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author AlexDing
 * @email alexding@gmail.com
 * @date 2021-10-23 01:48:30
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
