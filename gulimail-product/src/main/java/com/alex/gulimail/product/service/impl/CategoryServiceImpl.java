package com.alex.gulimail.product.service.impl;

import com.alex.gulimail.product.service.CategoryBrandRelationService;
import com.alex.gulimail.product.vo.CategoryRespVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.alex.common.utils.PageUtils;
import com.alex.common.utils.Query;

import com.alex.gulimail.product.dao.CategoryDao;
import com.alex.gulimail.product.entity.CategoryEntity;
import com.alex.gulimail.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryBrandRelationService relationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryRespVo> listWithTree() {
        //1.查出所有分类
        List<CategoryEntity> entityList = this.baseMapper.selectList(null);
        //将查询所有分类封装到vo对象
        List<CategoryRespVo> categoryRespVoList = entityList.stream().map(
                item -> {
                    CategoryRespVo categoryRespVo = new CategoryRespVo();
                    BeanUtils.copyProperties(item, categoryRespVo);
                    return categoryRespVo;
                }
        ).collect(Collectors.toList());

        //2.组装成父子树形结构
        //2.1 找到所有的一级分类
        List<CategoryRespVo> level1Menus = categoryRespVoList.stream()
                .filter(item -> item.getParentCid() == 0)
                .map(
                        menu -> {
                            menu.setChildren(getChildren(menu, categoryRespVoList));
                            return menu;
                        }
                )
                .sorted((menu1, menu2) ->
                        (menu1.getSort() == null ? 0 : menu1.getSort()) -
                                (menu2.getSort() == null ? 0 : menu2.getSort()))
                .collect(Collectors.toList());
        return level1Menus;
    }

    //递归查找所有菜单的子菜单
    private List<CategoryRespVo> getChildren(CategoryRespVo root, List<CategoryRespVo> all) {
        List<CategoryRespVo> children = all.stream()
                .filter(item -> item.getParentCid() == root.getCatId())
                //递归找到子菜单
                .map(
                        categoryRespVo -> {
                            categoryRespVo.setChildren(getChildren(categoryRespVo, all));
                            return categoryRespVo;
                        }
                )
                //菜单的排序
                .sorted((menu1, menu2) ->
                        (menu1.getSort() == null ? 0 : menu1.getSort()) -
                                (menu2.getSort() == null ? 0 : menu2.getSort()))
                .collect(Collectors.toList());
        return children;
    }

    @Override
    public void removeMenuByIds(List<Long> asList) {
        //TODO 1.检查当前删除的菜单，是否被别的地方引用
        //逻辑删除
        this.baseMapper.deleteBatchIds(asList);
    }

    /**
     * 找到catelogId完整路径
     *  [父/子/孙子]
     * @param catelogId
     * @return
     */
    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        paths = findParentPath(catelogId, paths);
        //递归查出来路径是相反的，使用集合工具类将顺序反转为：2,34,225
        Collections.reverse(paths);
        return paths.toArray(new Long[paths.size()]);
    }

    /**
     * 级联更新所有关联数据
     * @param category
     */
    @Transactional
    @Override
    public void updateCascade(CategoryEntity category) {
        this.baseMapper.updateById(category);
        relationService.updateCategory(category.getCatId(),category.getName());
    }

    //递归查出来数组：225,34,2
    private List<Long> findParentPath(Long catelogId,List<Long> paths){
        //收集当前节点id
        paths.add(catelogId);
        CategoryEntity entity = this.getById(catelogId);
        if (entity.getParentCid() != 0){
            findParentPath(entity.getParentCid(),paths);
        }
        return paths;
    }

}