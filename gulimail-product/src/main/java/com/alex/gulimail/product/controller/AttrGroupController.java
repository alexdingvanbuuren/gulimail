package com.alex.gulimail.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.alex.gulimail.product.entity.AttrEntity;
import com.alex.gulimail.product.service.AttrAttrgroupRelationService;
import com.alex.gulimail.product.service.AttrService;
import com.alex.gulimail.product.service.CategoryService;
import com.alex.gulimail.product.vo.AttrGroupRelationVo;
import com.alex.gulimail.product.vo.AttrGroupRespVo;
import com.alex.gulimail.product.vo.AttrGroupWithAttrsVo;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.alex.gulimail.product.entity.AttrGroupEntity;
import com.alex.gulimail.product.service.AttrGroupService;
import com.alex.common.utils.PageUtils;
import com.alex.common.utils.R;



/**
 * 属性分组
 *
 * @author AlexDing
 * @email alexding@gmail.com
 * @date 2021-10-23 02:11:52
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrService attrService;

    @Autowired
    private AttrAttrgroupRelationService relationService;

    /**
     * 获取分类下所有分组&关联属性
     * 请求路径：`/product/attrgroup/{catelogId}/withattr`
     */
    @GetMapping("/{catelogId}/withattr")
    public R getAttrGroupWithAttrs(@PathVariable("catelogId") Long catelogId){
        List<AttrGroupWithAttrsVo> vos = attrGroupService.getAttrGroupWithAttrsByCatelogId(catelogId);
        return R.ok().put("data",vos);
    }

    /**
     * 请求路径：`/product/attrgroup/attr/relation`
     */
    @PostMapping("/attr/relation")
    public R addAttrRelation(@RequestBody List<AttrGroupRelationVo> vos){
        relationService.saveBatchRelation(vos);
        return R.ok();
    }

    /**
     * 请求路径：`/product/attrgroup/attr/relation/delete`
     */
    @PostMapping("/attr/relation/delete")
    public R deleteRelation(@RequestBody AttrGroupRelationVo[] vos){
        attrService.deleteRelation(vos);
        return R.ok();
    }

    /**
     * 请求路径：`/product/attrgroup/{attrgroupId}/noattr/relation`
     */
    @GetMapping("/{attrgroupId}/noattr/relation")
    public R attrNoRelation(@RequestParam Map<String, Object> params,
                            @PathVariable("attrgroupId") Long attrgroupId){
        PageUtils page = attrService.getNoRelationAttr(params,attrgroupId);
        return R.ok().put("page",page);
    }

    /**
     * 请求路径：`/product/attrgroup/{attrgroupId}/attr/relation`
     */
    @GetMapping("/{attrgroupId}/attr/relation")
    public R attrRelation(@PathVariable("attrgroupId") Long attrgroupId){
        List<AttrEntity> data = attrService.getRelationAttr(attrgroupId);
        return R.ok().put("data",data);
    }

    /**
     * 请求路径：/product/attrgroup/list/{catelogId}
     */
    @GetMapping("/list/{catelogId}")
    public R findAttrGroupByCatelogId(@RequestParam Map<String, Object> params,
                                      @PathVariable("catelogId") Long catelogId){
        PageUtils page = attrGroupService.queryPage(params,catelogId);
        return R.ok().put("page",page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrGroupService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    //@RequiresPermissions("product:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
        AttrGroupRespVo vo = new AttrGroupRespVo();
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
        BeanUtils.copyProperties(attrGroup,vo);

        Long catelogId = vo.getCatelogId();
        Long[] path = categoryService.findCatelogPath(catelogId);
        vo.setCatelogPath(path);

        return R.ok().put("attrGroup", vo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attrgroup:save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
