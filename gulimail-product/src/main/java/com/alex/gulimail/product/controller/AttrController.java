package com.alex.gulimail.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.alex.gulimail.product.entity.ProductAttrValueEntity;
import com.alex.gulimail.product.service.ProductAttrValueService;
import com.alex.gulimail.product.vo.AttrRespVo;
import com.alex.gulimail.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.alex.gulimail.product.entity.AttrEntity;
import com.alex.gulimail.product.service.AttrService;
import com.alex.common.utils.PageUtils;
import com.alex.common.utils.R;



/**
 * 商品属性
 *
 * @author AlexDing
 * @email alexding@gmail.com
 * @date 2021-10-23 02:11:52
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;

    @Autowired
    private ProductAttrValueService productAttrValueService;

    /**
     * 获取spu规格
     * 请求路径：`/product/attr/base/listforspu/{spuId}`
     */
    @GetMapping("/base/listforspu/{spuId}")
    public R baseAttrForSpu(@PathVariable("spuId") Long spuId){
        List<ProductAttrValueEntity> data =  productAttrValueService.baseAttrValuesBySpuId(spuId);
        return R.ok().put("data", data);
    }


    /**
     * 请求路径：`/product/attr/base/list/{catelogId}`
     * 或
     * 请求路径：`/product/attr/sale/list/{catelogId}`
     */
    @GetMapping("/{attrType}/list/{catelogId}")
    //@RequiresPermissions("product:attr:list")
    public R baseAttrList(@RequestParam Map<String, Object> params,
                          @PathVariable("catelogId") Long catelogId,
                          @PathVariable("attrType") String attrType){
        PageUtils page = attrService.queryBaseAttrPage(params,catelogId,attrType);
        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("product:attr:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
    //@RequiresPermissions("product:attr:info")
    public R info(@PathVariable("attrId") Long attrId){
		AttrRespVo respVo = attrService.getAttrInfo(attrId);
        return R.ok().put("attr", respVo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attr:save")
    public R save(@RequestBody AttrVo attr){
		attrService.saveAttr(attr);
        return R.ok();
    }

    /**
     * 修改商品规格
     * 请求路径：`/product/attr/update/{spuId}`
     */
    @PostMapping("/update/{spuId}")
    //@RequiresPermissions("product:attr:update")
    public R updateSpuAttrValue(@PathVariable("spuId") Long spuId,
                                @RequestBody List<ProductAttrValueEntity> attrValueEntities){
        productAttrValueService.updateAttrValueBySpuId(spuId,attrValueEntities);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attr:update")
    public R update(@RequestBody AttrVo attr){
		attrService.updateAttr(attr);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attr:delete")
    public R delete(@RequestBody Long[] attrIds){
		attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
