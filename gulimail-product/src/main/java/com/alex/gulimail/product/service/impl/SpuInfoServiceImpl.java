package com.alex.gulimail.product.service.impl;

import com.alex.common.to.SkuReductionTo;
import com.alex.common.to.SpuBoundsTo;
import com.alex.common.utils.R;
import com.alex.gulimail.product.entity.*;
import com.alex.gulimail.product.feign.CouponServiceFeign;
import com.alex.gulimail.product.service.*;
import com.alex.gulimail.product.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.alex.common.utils.PageUtils;
import com.alex.common.utils.Query;

import com.alex.gulimail.product.dao.SpuInfoDao;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Autowired
    private SpuInfoDescService spuInfoDescService;

    @Autowired
    private SpuImagesService imagesService;

    @Autowired
    private ProductAttrValueService productAttrValueService;

    @Autowired
    private SkuInfoService skuInfoService;

    @Autowired
    private SkuImagesService skuImagesService;

    @Autowired
    private SkuSaleAttrValueService skuSaleAttrValueService;

    @Autowired
    private CouponServiceFeign couponServiceFeign;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private BrandService brandService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                new QueryWrapper<SpuInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * TODO 高级部分完善
     * @param vo
     */
    @Transactional
    @Override
    public void saveSpuInfo(SpuSaveVo vo) {
        //1.保存SPU基本信息，pms_spu_info
        SpuInfoEntity spuInfoEntity = new SpuInfoEntity();
        BeanUtils.copyProperties(vo,spuInfoEntity);
        spuInfoEntity.setCreateTime(new Date());
        spuInfoEntity.setUpdateTime(new Date());
        this.saveBaseSpuInfo(spuInfoEntity);

        Long spuId = spuInfoEntity.getId();
        Long catalogId = spuInfoEntity.getCatalogId();
        Long brandId = spuInfoEntity.getBrandId();

        //2.保存SPU描述图片，pms_spu_info_desc
        List<String> decript = vo.getDecript();
        SpuInfoDescEntity descEntity = new SpuInfoDescEntity();
        descEntity.setSpuId(spuId);
        descEntity.setDecript(String.join(",",decript));
        spuInfoDescService.saveSpuInfoDesc(descEntity);

        //3.保存SPU图片集，pms_spu_images
        List<String> images = vo.getImages();
        imagesService.saveImages(spuId,images);

        //4.保存SPU的规格参数，pms_product_attr_value
        List<BaseAttrs> baseAttrs = vo.getBaseAttrs();
        productAttrValueService.saveProductAttrs(spuId,baseAttrs);

        //5.保存SPU积分信息，操作的库：gulimail_sms
        //操作的库：gulimail_sms，sms_spu_bounds：积分表
        Bounds bounds = vo.getBounds();
        SpuBoundsTo spuBoundsTo = new SpuBoundsTo();
        BeanUtils.copyProperties(bounds,spuBoundsTo);
        spuBoundsTo.setSpuId(spuId);
        R r = couponServiceFeign.saveSpuBounds(spuBoundsTo);
        if(r.getCode() != 0){
            log.error("远程spu优惠信息失败");
        }

        //6.保存当前SPU对应的所有SKU信息
        List<Skus> skus = vo.getSkus();
        if(skus != null && skus.size() > 0){
            skus.stream().forEach(sku -> {
                //找到SKU默认图片
                List<Images> imgs = sku.getImages();
                String defaultImg = "";
                for (Images img : imgs) {
                    if(img.getDefaultImg() == 1){
                        defaultImg = img.getImgUrl();
                    }
                }

                //6.1 保存SKU基本信息，pms_sku_info
                SkuInfoEntity skuInfoEntity = new SkuInfoEntity();
                BeanUtils.copyProperties(sku,skuInfoEntity);
                skuInfoEntity.setSpuId(spuId);
                skuInfoEntity.setBrandId(brandId);
                skuInfoEntity.setCatalogId(catalogId);
                skuInfoEntity.setSaleCount(0L);
                skuInfoEntity.setSkuDefaultImg(defaultImg);
                skuInfoService.saveSkuInfo(skuInfoEntity);

                Long skuId = skuInfoEntity.getSkuId();

                //6.2 保存SKU图片集，pms_spu_images
                if(imgs != null && imgs.size() > 0){
                    List<SkuImagesEntity> imagesEntities = imgs.stream().map(img -> {
                        SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
                        BeanUtils.copyProperties(img, skuImagesEntity);
                        skuImagesEntity.setSkuId(skuId);
                        return skuImagesEntity;
                    }).filter(img -> {
                        //返回true需要，false剔除
                        return !StringUtils.isEmpty(img.getImgUrl());
                    }).collect(Collectors.toList());
                    skuImagesService.saveSkuImages(imagesEntities);
                }

                //6.3 保存SKU销售属性，pms_sku_sale_attr_value
                List<Attr> saleAttrs = sku.getAttr();
                if(saleAttrs != null && saleAttrs.size() > 0){
                    List<SkuSaleAttrValueEntity> saleAttrValueEntities = saleAttrs.stream().map(saleAttr -> {
                        SkuSaleAttrValueEntity saleAttrValueEntity = new SkuSaleAttrValueEntity();
                        BeanUtils.copyProperties(saleAttr, saleAttrValueEntity);
                        saleAttrValueEntity.setSkuId(skuId);
                        return saleAttrValueEntity;
                    }).collect(Collectors.toList());
                    skuSaleAttrValueService.saveSkuSaleAttrs(saleAttrValueEntities);
                }

                //6.4 保存SKU的优惠、满减等信息，操作的库：gulimail_sms
                //操作的库：gulimail_sms，sms_sku_ladder：打折表
                //操作的库：gulimail_sms，sms_sku_full_reduction：满减表
                //操作的库：gulimail_sms，sms_member_price：会员价格表
                SkuReductionTo skuReductionTo = new SkuReductionTo();
                BeanUtils.copyProperties(sku,skuReductionTo);
                System.out.println(skuReductionTo);
                skuReductionTo.setSkuId(skuId);
                if(skuReductionTo.getFullCount() > 0 || skuReductionTo.getFullPrice().compareTo(BigDecimal.ZERO) == 1){
                    R r1 = couponServiceFeign.saveSkuReduction(skuReductionTo);
                    if(r1.getCode() != 0){
                        log.error("远程sku优惠、满减等信息失败");
                    }
                }

            });
        }



    }

    @Override
    public void saveBaseSpuInfo(SpuInfoEntity spuInfoEntity) {
        this.save(spuInfoEntity);
    }

    /**
     * 需要检索的条件：
     *    key: '华为',//检索关键字
     *    catelogId: 6,//三级分类id
     *    brandId: 1,//品牌id
     *    status: 0,//商品状态
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        QueryWrapper<SpuInfoEntity> queryWrapper = new QueryWrapper<>();

        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            queryWrapper.and(wrapper -> {
                wrapper.eq("id",key).or().like("spu_name",key);
            });
        }

        String catelogId = (String) params.get("catelogId");
        if(!StringUtils.isEmpty(catelogId) && !"0".equalsIgnoreCase(catelogId)){
            queryWrapper.eq("catalog_id",catelogId);
        }

        String brandId = (String) params.get("brandId");
        if(!StringUtils.isEmpty(brandId) && !"0".equalsIgnoreCase(brandId)){
            queryWrapper.eq("brand_id",brandId);
        }

        String status = (String) params.get("status");
        if(!StringUtils.isEmpty(status)){
            queryWrapper.eq("publish_status",status);
        }

        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params), queryWrapper);
        PageUtils pageUtils = new PageUtils(page);
        List<SpuInfoEntity> records = page.getRecords();
        List<SpuInfoRespVo> collect = records.stream().map(item -> {
            SpuInfoRespVo spuInfoRespVo = new SpuInfoRespVo();
            BeanUtils.copyProperties(item, spuInfoRespVo);
            BrandEntity brandEntity = brandService.getById(item.getBrandId());
            CategoryEntity categoryEntity = categoryService.getById(item.getCatalogId());
            spuInfoRespVo.setBrandName(brandEntity.getName());
            spuInfoRespVo.setCatalogName(categoryEntity.getName());
            return spuInfoRespVo;
        }).collect(Collectors.toList());
        pageUtils.setList(collect);
        return pageUtils;
    }

}