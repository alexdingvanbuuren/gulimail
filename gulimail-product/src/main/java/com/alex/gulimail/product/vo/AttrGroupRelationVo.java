package com.alex.gulimail.product.vo;

import lombok.Data;

/**
 * @author AlexDing
 * @create 2021-10-31 下午 18:27
 */
@Data
public class AttrGroupRelationVo {

    //[{"attrId":1,"attrGroupId":2}]
    private Long attrId;
    private Long attrGroupId;
}
