package com.alex.gulimail.product.vo;

import lombok.Data;

/**
 * @author AlexDing
 * @create 2021-10-31 下午 14:35
 */
@Data
public class AttrRespVo extends AttrVo{

    /**
     * 分类名称
     */
    private String catelogName;
    /**
     * 分组名称
     */
    private String groupName;

    /**
     * 分类完整路径
     */
    private Long[] catelogPath;
}
