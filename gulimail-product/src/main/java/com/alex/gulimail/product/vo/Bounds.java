/**
  * Copyright 2021 bejson.com 
  */
package com.alex.gulimail.product.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2021-11-01 1:4:36
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Bounds {
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}