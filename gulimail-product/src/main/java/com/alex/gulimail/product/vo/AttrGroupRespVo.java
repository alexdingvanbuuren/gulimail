package com.alex.gulimail.product.vo;


import lombok.Data;

/**
 * @author AlexDing
 * @create 2021-10-30 下午 20:50
 */
@Data
public class AttrGroupRespVo {

    /**
     * 分组id
     */
    private Long attrGroupId;
    /**
     * 组名
     */
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    private String icon;
    /**
     * 所属分类id
     */
    private Long catelogId;

    /**
     * 分组属性完整路径
     */
    private Long[] catelogPath;
}
