package com.alex.gulimail.product.service.impl;

import com.alex.gulimail.product.entity.AttrEntity;
import com.alex.gulimail.product.service.AttrService;
import com.alex.gulimail.product.vo.BaseAttrs;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.alex.common.utils.PageUtils;
import com.alex.common.utils.Query;

import com.alex.gulimail.product.dao.ProductAttrValueDao;
import com.alex.gulimail.product.entity.ProductAttrValueEntity;
import com.alex.gulimail.product.service.ProductAttrValueService;
import org.springframework.transaction.annotation.Transactional;


@Service("productAttrValueService")
public class ProductAttrValueServiceImpl extends ServiceImpl<ProductAttrValueDao, ProductAttrValueEntity> implements ProductAttrValueService {

    @Autowired
    private AttrService attrService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProductAttrValueEntity> page = this.page(
                new Query<ProductAttrValueEntity>().getPage(params),
                new QueryWrapper<ProductAttrValueEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveProductAttrs(Long spuId, List<BaseAttrs> baseAttrs) {
        if(baseAttrs == null || baseAttrs.size() == 0){
            return;
        }
        List<ProductAttrValueEntity> valueEntities = baseAttrs.stream().map(attr -> {
            ProductAttrValueEntity attrValueEntity = new ProductAttrValueEntity();
            attrValueEntity.setSpuId(spuId);
            attrValueEntity.setAttrId(attr.getAttrId());
            attrValueEntity.setAttrValue(attr.getAttrValues());
            AttrEntity attrEntity = attrService.getById(attr.getAttrId());
            attrValueEntity.setAttrName(attrEntity.getAttrName());
            attrValueEntity.setQuickShow(attr.getShowDesc());
            return attrValueEntity;
        }).collect(Collectors.toList());
        this.saveBatch(valueEntities);
    }

    @Override
    public List<ProductAttrValueEntity> baseAttrValuesBySpuId(Long spuId) {
        List<ProductAttrValueEntity> valueEntities = this.list(
                new QueryWrapper<ProductAttrValueEntity>().eq("spu_id", spuId));
        return valueEntities;
    }

    @Transactional
    @Override
    public void updateAttrValueBySpuId(Long spuId, List<ProductAttrValueEntity> attrValueEntities) {
        //1.删除spuId之前对应的规格属性
        this.baseMapper.delete(new QueryWrapper<ProductAttrValueEntity>().eq("spu_id",spuId));
        //2.再重新添加spu对应的规格属性
        List<ProductAttrValueEntity> collect = attrValueEntities.stream().map(item -> {
            item.setSpuId(spuId);
            return item;
        }).collect(Collectors.toList());
        this.saveBatch(collect);
    }

}