package com.alex.gulimail.product.vo;

import lombok.Data;

/**
 * @author AlexDing
 * @create 2021-10-31 下午 23:17
 */
@Data
public class BrandVo {
    private Long brandId;
    private String brandName;
}
