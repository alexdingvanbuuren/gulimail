package com.alex.gulimail.product.feign;

import com.alex.common.to.SkuReductionTo;
import com.alex.common.to.SpuBoundsTo;
import com.alex.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author AlexDing
 * @create 2021-11-02 上午 0:24
 */
@FeignClient("gulimail-coupon")
public interface CouponServiceFeign {

    /**
     * 1.CouponFeignService.saveSpuBounds(SpuBoundsTo);
     *      1）、将这个对象转为json.
     *      2）、找到gulimail-coupon服务，给/coupon/spubounds/save 发送请求。
     *           将上一步转的json放在请求体位置，发送请求
     *      3）、对方服务收到请求。其实是收到请求体里json数据
     *          (@RequestBody SpuBoundsEntity spuBounds); 将请求体的json转为SpuBoundsEntity
     * 总结：只要json数据模型是兼容的，双方服务无需使用同一个to
     * @param spuBoundsTo
     * @return
     */
    @PostMapping("/coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBoundsTo spuBoundsTo);

    @PostMapping("coupon/skufullreduction/saveInfo")
    R saveSkuReduction(@RequestBody SkuReductionTo skuReductionTo);
}
