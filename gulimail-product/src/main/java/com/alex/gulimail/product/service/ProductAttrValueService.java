package com.alex.gulimail.product.service;

import com.alex.gulimail.product.vo.BaseAttrs;
import com.baomidou.mybatisplus.extension.service.IService;
import com.alex.common.utils.PageUtils;
import com.alex.gulimail.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author AlexDing
 * @email alexding@gmail.com
 * @date 2021-10-23 01:48:30
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveProductAttrs(Long spuId, List<BaseAttrs> baseAttrs);

    List<ProductAttrValueEntity> baseAttrValuesBySpuId(Long spuId);

    void updateAttrValueBySpuId(Long spuId, List<ProductAttrValueEntity> attrValueEntities);
}

