package com.alex.gulimail.member.dao;

import com.alex.gulimail.member.entity.MemberCollectSubjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的专题活动
 * 
 * @author AlexDing
 * @email alexding@gmail.com
 * @date 2021-10-23 13:24:22
 */
@Mapper
public interface MemberCollectSubjectDao extends BaseMapper<MemberCollectSubjectEntity> {
	
}
