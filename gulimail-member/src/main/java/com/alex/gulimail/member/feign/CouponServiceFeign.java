package com.alex.gulimail.member.feign;

import com.alex.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 这是一个声明式远程调用
 */
@FeignClient("gulimail-coupon")
public interface CouponServiceFeign {

    @RequestMapping("/coupon/coupon/member/list")
    public R memberCoupons();
}
