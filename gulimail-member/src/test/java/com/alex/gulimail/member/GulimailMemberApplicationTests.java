package com.alex.gulimail.member;

import com.alex.common.utils.R;
import com.alex.gulimail.member.feign.CouponServiceFeign;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class GulimailMemberApplicationTests {

    @Autowired
    private CouponServiceFeign couponServiceFeign;

    @Test
    void contextLoads() {
        R memberCoupons = couponServiceFeign.memberCoupons();
        List coupons = (List) memberCoupons.get("coupons");
        System.out.println(coupons);
    }

}
