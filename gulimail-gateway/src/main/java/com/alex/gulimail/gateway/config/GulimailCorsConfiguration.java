package com.alex.gulimail.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

@Configuration
public class GulimailCorsConfiguration {

    @Bean
    public CorsWebFilter corsWebFilter(){
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();

        //1.配置跨域
        //允许哪些头信息跨域 *表示任意
        corsConfiguration.addAllowedHeader("*");
        //允许哪些请求方式跨域 *表示任意
        corsConfiguration.addAllowedMethod("*");
        //允许哪些请求来源跨域 *表示任意
        corsConfiguration.addAllowedOrigin("*");
        //是否允许携带cookie进行跨域 true允许 false不允许
        corsConfiguration.setAllowCredentials(true);

        //配置 /** 表示任意路径都需要跨域配置
        source.registerCorsConfiguration("/**",corsConfiguration);
        return new CorsWebFilter(source);
    }
}
